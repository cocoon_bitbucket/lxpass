package lesspass

import (
	"crypto/sha256"
	"fmt"
	"math/big"

	"golang.org/x/crypto/pbkdf2"
)

var CHARACTER_SUBSETS = map[string]string{
	"lowercase": "abcdefghijklmnopqrstuvwxyz",
	"uppercase": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	"digits":    "0123456789",
	"symbols":   "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~",
}

type Entropy struct {
	// entropy is a big integer
	*big.Int
}

// NewEntropy create an Entropy and initialize its value to 0
func NewEntropy() *Entropy {
	i := big.NewInt(0)
	entropy := &Entropy{i}
	return entropy
}

// SetEntropy : compute entropy from passowrd profile and master password
func (e *Entropy) SetEntropy(profile *Profile, masterPassword string) {

	// compute salt:  site + login + counter
	var salt string
	salt = profile.Site + profile.Login
	h := fmt.Sprintf("%x", profile.Counter)
	salt = salt + h
	dk := pbkdf2.Key([]byte(masterPassword), []byte(salt), 100000, 32, sha256.New)
	hexStr := fmt.Sprintf("%x", dk)
	entropy, ok := new(big.Int).SetString(hexStr, 16)
	if !ok {
		// fail : set to 0
		entropy = new(big.Int).SetInt64(0)
	}
	e.Int = entropy
}

// GetEntropy get the value : a big integer
func (e *Entropy) GetEntropy() *big.Int {
	return e.Int
}

type Profile struct {
	Site      string //"site": "site",
	Login     string //"login": "login",
	LowerCase bool   //"lowercase": True,
	UpperCase bool   //"uppercase": True,
	Digits    bool   //"digits": True,
	Symbols   bool   //"symbols": True,
	Length    int    //"length": 16,
	Counter   int    // "counter": 10,
}

// NewProfile : create a blank password profile
func NewProfile() *Profile {
	p := &Profile{
		Site:      "site",
		Login:     "login",
		LowerCase: true,
		UpperCase: true,
		Digits:    true,
		Symbols:   true,
		Length:    16,
		Counter:   10,
	}
	return p
}

// CalcEntropy : compute entropy
func CalcEntropy(profile *Profile, masterPassword string) (entropy *big.Int) {

	// compute salt:  site + login + counter
	var salt string
	salt = profile.Site + profile.Login
	h := fmt.Sprintf("%x", profile.Counter)
	salt = salt + h
	dk := pbkdf2.Key([]byte(masterPassword), []byte(salt), 100000, 32, sha256.New)
	hexStr := fmt.Sprintf("%x", dk)
	entropy, ok := new(big.Int).SetString(hexStr, 16)
	if !ok {
		entropy = new(big.Int).SetInt64(0)
	}
	return entropy
}

func GetConfiguredRules(profile *Profile) (rules []string) {

	// allRules : "lowercase", "uppercase", "digits", "symbols"
	if profile.LowerCase {
		rules = append(rules, "lowercase")
	}
	if profile.UpperCase {
		rules = append(rules, "uppercase")
	}
	if profile.Digits {
		rules = append(rules, "digits")
	}
	if profile.Symbols {
		rules = append(rules, "symbols")
	}
	return rules
}

func GetSetOfCharacters(rules []string) (chars string) {
	if len(rules) == 0 {
		chars = CHARACTER_SUBSETS["lowercase"] + CHARACTER_SUBSETS["uppercase"] +
			CHARACTER_SUBSETS["digits"] + CHARACTER_SUBSETS["symbols"]

	} else {
		for _, rule := range rules {
			chars = chars + CHARACTER_SUBSETS[rule]
		}
	}
	return chars
}

func GetOneCharPerRule(entropy *big.Int, rules []string) (oneCharPerRule string, entro *big.Int) {
	oneCharPerRule = ""
	value := ""
	for _, rule := range rules {
		value, entropy = ConsumeEntropy("", entropy, CHARACTER_SUBSETS[rule], 1)
		oneCharPerRule = oneCharPerRule + value
	}
	return oneCharPerRule, entropy
}

func InsertStringPseudoRandomly(password string, entropy *big.Int, added string) string {

	generatedPassword := password
	modulo := big.NewInt(0)

	for _, letter := range added {
		//quotient, remainder =
		newQuot := new(big.Int)
		denominator := big.NewInt(int64(len(generatedPassword)))
		quotient, remainder := newQuot.DivMod(entropy, denominator, modulo)
		//println(modulo.String())
		entropy = quotient
		index := remainder.Int64()
		generatedPassword = generatedPassword[:index] + string(letter) + generatedPassword[index:]
	}
	return generatedPassword
}

func ConsumeEntropy(password string, quotient *big.Int, characters string, maxLength int) (pass string, quot *big.Int) {

	if len(password) >= maxLength {
		return password, quotient
	}
	denominator := big.NewInt(int64(len(characters)))
	modulo := big.NewInt(0)
	newQuot := new(big.Int)
	q, remainder := newQuot.DivMod(quotient, denominator, modulo)
	password = password + string(characters[remainder.Int64()])

	return ConsumeEntropy(password, q, characters, maxLength)
}

func RenderPassword(entropy *big.Int, profile *Profile) string {

	rules := GetConfiguredRules(profile)
	characters := GetSetOfCharacters(rules)

	password, passwordEntropy := ConsumeEntropy(
		"",
		entropy,
		characters,
		profile.Length-len(rules))
	added, characterEntropy := GetOneCharPerRule(passwordEntropy, rules)

	result := InsertStringPseudoRandomly(password, characterEntropy, added)

	return result
}

func GeneratePassword(profile *Profile, password string) string {

	entropy := NewEntropy()
	entropy.SetEntropy(profile, password)

	return RenderPassword(entropy.GetEntropy(), profile)

}
