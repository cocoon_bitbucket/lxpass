package lesspass

import (
	"math/big"
	"testing"
)

func SampleEntropy() *Entropy {
	e := NewEntropy()
	e.SetInt64(1234)
	return e

}

func TestEntropyType(t *testing.T) {

	en1 := big.NewInt(125)
	if en1.Text(10) != "125" {
		t.Fail()
	}

	en2 := NewEntropy()
	if en2.Text(10) != "0" {
		t.Fail()
	}
	en2.SetInt64(12)
	if en2.Text(10) != "12" {
		t.Fail()
	}
	if en2.Text(16) != "c" {
		t.Fail()
	}

	var en3 *Entropy

	en3 = SampleEntropy()
	if en3.Text(10) != "1234" {
		t.Fail()
	}

}

func TestSetEntropy(t *testing.T) {

	p := NewProfile()
	p.Site = "example.org"
	p.Login = "contact@example.org"
	p.Counter = 1
	password := "password"

	entropy := NewEntropy()
	entropy.SetEntropy(p, password)

	if entropy.Text(16) != "dc33d431bce2b01182c613382483ccdb0e2f66482cbba5e9d07dab34acc7eb1e" {
		t.Fail()
	}
}

func TestCalcEntropy(t *testing.T) {

	p := NewProfile()
	p.Site = "example.org"
	p.Login = "contact@example.org"
	p.Counter = 1
	password := "password"

	entropy := CalcEntropy(p, password)
	if entropy.Text(16) != "dc33d431bce2b01182c613382483ccdb0e2f66482cbba5e9d07dab34acc7eb1e" {
		t.Fail()
	}
}

func Test_get_configured_rules_empty_when_no_rules_in_profile(t *testing.T) {
	profile := &Profile{}
	rules := GetConfiguredRules(profile)
	if len(rules) != 0 {
		t.Fail()
		return
	}
}

func Test_get_configured_rules_ignore_disable_rules(t *testing.T) {
	profile := &Profile{}
	profile.LowerCase = false
	profile.UpperCase = true
	profile.Digits = false
	profile.Symbols = true
	rules := GetConfiguredRules(profile)
	if len(rules) == 2 {
		if rules[0] == "uppercase" && rules[1] == "symbols" {
			return
		}
	}
	t.Fail()
}

func Test_get_set_of_characters_without_rule(t *testing.T) {
	var rules []string
	chars := GetSetOfCharacters(rules)
	if chars != "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~" {
		t.Fail()
		return
	}
}

func Test_get_set_of_characters_with_single_rule(t *testing.T) {

	chars := GetSetOfCharacters([]string{"lowercase"})
	if chars != "abcdefghijklmnopqrstuvwxyz" {
		t.Fail()
		return
	}
	chars = GetSetOfCharacters([]string{"uppercase"})
	if chars != "ABCDEFGHIJKLMNOPQRSTUVWXYZ" {
		t.Fail()
		return
	}
	chars = GetSetOfCharacters([]string{"digits"})
	if chars != "0123456789" {
		t.Fail()
		return
	}
	chars = GetSetOfCharacters([]string{"symbols"})
	//if chars != "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~" {
	if chars != CHARACTER_SUBSETS["symbols"] {
		t.Fail()
		return
	}

}

func Test_get_set_of_characters_with_several_rules(t *testing.T) {
	chars := GetSetOfCharacters([]string{"lowercase", "digits"})
	if chars != "abcdefghijklmnopqrstuvwxyz0123456789" {
		t.Fail()
		return
	}
}

func Test_insert_string_pseudo_randomly(t *testing.T) {
	entropy := "80845849188608437656228503146902068601204454199548659454"
	var bigInt, ok = new(big.Int).SetString(entropy, 10)
	if !ok {
		t.Fail()
		return
	}
	pwd := InsertStringPseudoRandomly(
		"gsrwvjl3d0sn",
		bigInt,
		"a0",
	)
	if pwd != "gsrwvjl03d0asn" {
		t.Fail()
		return
	}
}

func Test_consume_entropy(t *testing.T) {

	entropy := "dc33d431bce2b01182c613382483ccdb0e2f66482cbba5e9d07dab34acc7eb1e"
	var bigInt, ok = new(big.Int).SetString(entropy, 16)
	if !ok {
		t.Fail()
		return
	}

	passwordValue, passwordEntropy := ConsumeEntropy(
		"",
		bigInt,
		"abcdefghijklmnopqrstuvwxyz0123456789",
		12,
	)
	if passwordValue != "gsrwvjl3d0sn" {
		t.Fail()
		return
	}
	refEntropy, ok := new(big.Int).SetString("21019920789038193790619410818194537836313158091882651458040", 10)
	if !ok {
		t.Fail()
		return
	}
	if passwordEntropy.String() != refEntropy.String() {
		t.Fail()
		return
	}
}

func Test_render_password(t *testing.T) {

	profile := &Profile{
		Site:      "example.org",
		Login:     "contact@example.org",
		Digits:    true,
		LowerCase: true,
		Length:    14,
		Counter:   1,
	}
	masterPassword := "password"
	entropy := NewEntropy()
	entropy.SetEntropy(profile, masterPassword)

	password := RenderPassword(entropy.GetEntropy(), profile)
	if password != "gsrwvjl03d0asn" {
		t.Fail()
		return
	}
}

func Test_generate_password(t *testing.T) {
	profile := &Profile{
		Site:      "example.org",
		Login:     "contact@example.org",
		LowerCase: true,
		UpperCase: true,
		Digits:    true,
		Symbols:   true,
		Length:    16,
		Counter:   1,
	}
	masterPassword := "password"
	password := GeneratePassword(profile, masterPassword)
	if password != "WHLpUL)e00[iHR+w" {
		t.Fail()
		return
	}
}

func Test_generate_password_2(t *testing.T) {
	profile := &Profile{
		Site:      "example.org",
		Login:     "contact@example.org",
		LowerCase: true,
		UpperCase: true,
		Digits:    true,
		Symbols:   false,
		Length:    14,
		Counter:   2,
	}
	masterPassword := "password"
	password := GeneratePassword(profile, masterPassword)
	if password != "MBAsB7b1Prt8Sl" {
		t.Fail()
	}
}

func Test_generate_password_3(t *testing.T) {
	profile := &Profile{
		Site:      "example.org",
		Login:     "contact@example.org",
		LowerCase: false,
		UpperCase: false,
		Digits:    true,
		Symbols:   false,
		Length:    16,
		Counter:   1,
	}
	masterPassword := "password"
	password := GeneratePassword(profile, masterPassword)
	if password != "8742368585200667" {
		t.Fail()
	}
}

func Test_generate_password_4(t *testing.T) {
	profile := &Profile{
		Site:      "example.org",
		Login:     "contact@example.org",
		LowerCase: true,
		UpperCase: true,
		Digits:    false,
		Symbols:   true,
		Length:    16,
		Counter:   1,
	}
	masterPassword := "password"
	password := GeneratePassword(profile, masterPassword)
	if password != "s>{F}RwkN/-fmM.X" {
		t.Fail()
	}
}

func Test__generate_password_nrt_328(t *testing.T) {
	profile := &Profile{
		Site:      "site",
		Login:     "login",
		LowerCase: true,
		UpperCase: true,
		Digits:    true,
		Symbols:   true,
		Length:    16,
		Counter:   10,
	}
	masterPassword := "test"
	password := GeneratePassword(profile, masterPassword)
	if password != "XFt0F*,r619:+}[." {
		t.Fail()
	}
}
